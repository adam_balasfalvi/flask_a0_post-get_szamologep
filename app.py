# https://code.visualstudio.com/docs/python/tutorial-flask

from flask import Flask
from flask import render_template
from flask import request
import Szamologep 

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/szamologep")
def szamologep():    
    return render_template("szamologep.html")

@app.route("/szamologep_post", methods=['POST'])
def szamologep_post():
    # https://stackoverflow.com/questions/19794695/flask-python-buttons
    if request.method == 'POST':
        szamologep = Szamologep.Szamologep()

        a = float(request.form.get('a'))
        b = float(request.form.get('b'))

        # https://stackoverflow.com/questions/10434599/get-the-data-received-in-a-flask-request
        if  request.form.get('plusz', '') == 'plusz':
            eredmeny = szamologep.plusz(a, b)
            
        if  request.form.get('minusz', '') == 'minusz':
            eredmeny = szamologep.minusz(a, b)
        
        if  request.form.get('szorzas', '') == 'szorzas':
            eredmeny = szamologep.szorzas(a, b)
        
        if  request.form.get('osztas', '') == 'osztas':
            eredmeny = szamologep.osztas(a, b)
    
    return render_template("szamologep.html", eredmeny = eredmeny)

@app.route("/szamologep_get", methods=['GET'])
def szamologep_get():
    if request.method == 'GET':
        szamologep = Szamologep.Szamologep()

        # https://stackoverflow.com/questions/24892035/how-can-i-get-the-named-parameters-from-a-url-using-flask
        a = float(request.args.get('a'))
        b = float(request.args.get('b'))
        eredmeny = None

        if request.args.get('plusz', ''):
            eredmeny = szamologep.plusz(a, b)
        
        if request.args.get('minusz', ''):
            eredmeny = szamologep.minusz(a, b)
        
        if request.args.get('szorzas', ''):
            eredmeny = szamologep.szorzas(a, b)

        if request.args.get('osztas', ''):
            eredmeny = szamologep.osztas(a, b)
        
        # https://stackoverflow.com/questions/664294/is-it-possible-only-to-declare-a-variable-without-assigning-any-value-in-python
        if eredmeny is None:
            eredmeny = 0

        return render_template('szamologep.html', eredmeny = eredmeny)
